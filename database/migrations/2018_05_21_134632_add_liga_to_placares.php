<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLigaToPlacares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
                Schema::table('placares', function (Blueprint $table) {

         $table->integer('liga_id')->nullable()->unsigned();

            $table->foreign('liga_id')
                    ->references('id')->on('ligas')
                    ->onDelete('cascade');

                            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
