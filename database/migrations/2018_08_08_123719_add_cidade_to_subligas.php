<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCidadeToSubligas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subligas', function (Blueprint $table) {

         $table->integer('cidade_id')->nullable()->unsigned();

            $table->foreign('cidade_id')
                    ->references('id')->on('cidades')
                    ->onDelete('cascade');

                            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
