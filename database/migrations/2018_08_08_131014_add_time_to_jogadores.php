<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeToJogadores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('jogadores', function (Blueprint $table) {

         $table->integer('time_id')->nullable()->unsigned();

            $table->foreign('time_id')
                    ->references('id')->on('times')
                    ->onDelete('cascade');

                            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
