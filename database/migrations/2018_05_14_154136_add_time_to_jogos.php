<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeToJogos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jogos', function (Blueprint $table) {
            $table->integer('time_casa_id')->nullable()->unsigned();

            $table->foreign('time_casa_id')
                    ->references('id')->on('times')
                    ->onDelete('cascade');


            $table->integer('time_adversario_id')->nullable()->unsigned();

            $table->foreign('time_adversario_id')
                    ->references('id')->on('times')
                    ->onDelete('cascade');


            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
