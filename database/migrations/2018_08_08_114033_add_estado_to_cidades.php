<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEstadoToCidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('cidades', function (Blueprint $table) {

         $table->integer('estado_id')->nullable()->unsigned();

            $table->foreign('estado_id')
                    ->references('id')->on('estados')
                    ->onDelete('cascade');

                            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
