@extends('principal')

@section('conteudo')

<div class='col-sm-11'>
    <h2> Jogadores </h2>
</div>
<div class='col-sm-1'>
    <br>
    <a href="#" class="btn btn-primary" 
       role="button">Ver Todos</a>
</div>

<form method="post" action="#">
    {{ csrf_field() }}

    <div class='col-sm-4'>
        <div class="form-group">
            <label for="nome_jogador">Pesquisa por Nome da Jogador:</label>
            <input type="text" class="form-control" id="nome_jogador"
                   name="nome_jogador">
        </div>
    </div>


    <div class='col-sm-4'>
        <label> &nbsp; </label>
        <button type="submit" class="btn btn-warning">Pesquisar</button>
        <a href="{{route('cadastra.jogadores')}}" class="btn btn-success">Novo Jogador</a>
                <a href="#" class="btn btn-info">Gerar Pdf</a>

    </div>    

</form>



<div class='col-sm-12'>

    @if (count($jogadores)==0)
    <div class="alert alert-danger">
        Não há jogadores com os filtros informados...
    </div>
    @endif

    <table class="table table-hover">
        <thead>
            <tr>
                <th>Código</th>
                <th>Nome do Jogador</th>
                <th style="text-align: center; color: #ccac00"><b>Cartões Amarelos ▉</b></th>
                <th style="text-align: center; color: #cc0000"><b>Cartões Vermelhos ▉</b></th>
                <th>Nome do Time</th>
                <th>Nome da Liga</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($jogadores as $jogador)
            <tr>
                <td style="text-align: center">{{$jogador->id}}</td>
                <td>{{$jogador->nome_jogador}}</td>




                <td style="text-align: center; color: #ccac00"><a href="{{route('remover.cartao.amarelo', $jogador->id)}}" class="btn btn-warning btn-sm">-1</a> &nbsp;<b>{{$jogador->cartao_amarelo}}</b>&nbsp; <a href="{{route('adicionar.cartao.amarelo', $jogador->id)}}" class="btn btn-warning btn-sm">+1</a> </td>





                <td style="text-align: center; color: #cc0000"><a href="{{route('remover.cartao.vermelho', $jogador->id)}}" class="btn btn-danger btn-sm">-1</a> &nbsp;<b>{{$jogador->cartao_vermelho}}</b> &nbsp;<a href="{{route('adicionar.cartao.vermelho', $jogador->id)}}" class="btn btn-danger btn-sm">+1</a></td>



                
                <td>{{$jogador->times->nome_time}}</td>
                <td>{{$jogador->times->ligas->nome_liga}}</td>


                <td>
                    
                    <form style="display: inline-block"
                          method="post"
                          action="{{route('deletar.jogador', $jogador->id)}}"
                          onsubmit="return confirm('Confirma Exclusão do Jogador?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger"> Excluir </button>
                    </form> &nbsp;&nbsp;

                </td>
                @endforeach
            </tr>


        </tbody>
    </table>    

                    {{ $jogadores->links() }}



</div>

@endsection