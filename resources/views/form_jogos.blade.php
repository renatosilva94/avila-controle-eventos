@extends('principal')

@section('conteudo')


<div class='col-sm-11'>
@if ($acao == 1)
    <h2> Inclusão de Jogos </h2>
@else
    <h2> Alteração de Jogos </h2>
@endif
</div>
<div class='col-sm-1'>
    <a href="#" class="btn btn-primary" 
       role="button">Voltar</a>
</div>

<div class='col-sm-12'>
    
@if ($acao == 1)
    <form method="post" action="{{route('salvar.jogo')}}">
@else
    <form method="post" action="#">
@endif
        {{ csrf_field() }}




<div class="form-group">
            <label for="data_jogo">Data do Jogo:</label>
            <input type="text" class="form-control" id="data_jogo"
                   name="data_jogo"
                   required value='<?php echo (new \DateTime())->format('Y-m-d'); ?>'>
        </div>

         <div class="form-group">
            <label for="liga_id">Nome da Liga Que o Jogo Se Enquadra:</label>
            <select class="form-control" id="liga_id" name="liga_id">
            <option></option>
            @foreach($ligas as $liga)
            <option value="{{$liga->id}}" name="nome_liga">{{$liga->nome_liga}}</option>
            @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="time_casa_id">Time da Casa:</label>
            <select class="form-control" id="time_id" name="time_casa_id" disabled="disabled">
            <option></option>
            @foreach($times as $time)
            <option value="{{$time->id}}" name="time_casa_id">{{$time->nome_time}}</option>
            @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="placar_time_casa">Placar do Time da Casa:</label>
            <input type="number" class="form-control" id="placar_time_casa"
                   name="placar_time_casa" 
                   value="{{$jogo->placar_time_casa or old('placar_time_casa')}}"
                   required>
        </div>

         <div class="form-group">
            <label for="time_adversario_id">Time Adversário:</label>
            <select class="form-control" id="time_id_2" name="time_adversario_id" disabled="disabled">
            <option></option>
            @foreach($times as $time)
            <option value="{{$time->id}}" name="time_adversario_id">{{$time->nome_time}}</option>
            @endforeach
            </select>
        </div>

                <div class="form-group">
            <label for="placar_time_adversario">Placar do Time Adversário:</label>
            <input type="number" class="form-control" id="placar_time_adversario"
                   name="placar_time_adversario" 
                   value="{{$jogo->placar_time_adversario or old('placar_time_adversario')}}"
                   required>
        </div>

              
<script>

/*
$(function() {
  $('#time_id').change(function() {
    dropdownval = $(this).val();
    $('#time_id_2').not(this).find('option[value="' + dropdownval + '"]').hide();
  });
});
*/






                                $('#liga_id').on('change', function () {
                                    var ligaID = $(this).val();
                                    if (ligaID) {
                                        $.ajax({
                                            type: "GET",
                                            url: "{{url('ajax/pegar-times-liga')}}?liga_id=" + ligaID,
                                            success: function (res) {
                                                if (res) {
                                                    $("#time_id").empty();
                                                    $.each(res, function (key, value) {
                                                        $("#time_id").append('<option value="' + key + '">' + value + '</option>');

                                                                $("#time_id").removeAttr("disabled");



                                                               






                                                    });
                                                    
                                                } else {
                                                    $("#time_id").empty();
                                                }
                                            }
                                        });
                                    } else {
                                        $("#time_id").empty();
                                    }

                                });




$('#liga_id').on('change', function () {
                                    var ligaID = $(this).val();
                                    if (ligaID) {
                                        $.ajax({
                                            type: "GET",
                                            url: "{{url('ajax/pegar-times-liga')}}?liga_id=" + ligaID,
                                            success: function (res) {
                                                if (res) {
                                                    $("#time_id_2").empty();
                                                    $.each(res, function (key, value) {
                                                        $("#time_id_2").append('<option value="' + key + '">' + value + '</option>');

                                                                $("#time_id_2").removeAttr("disabled");


                                                    });
                                                    
                                                } else {
                                                    $("#time_id_2").empty();
                                                }
                                            }
                                        });
                                    } else {
                                        $("#time_id_2").empty();
                                    }

                                });








</script>




      
        <button type="submit" class="btn btn-primary">Salvar</button>        
        <button type="reset" class="btn btn-warning">Limpar</button>        
    </form>    
</div>    

@endsection