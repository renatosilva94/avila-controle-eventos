<!DOCTYPE html>
<html lang="en">
    <head>
        <title> Avila Eventos Esportivos </title>

<link rel="shortcut icon" href="{{asset('icones/favicon.ico')}}" >


        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="css/lightbox.css" rel="stylesheet">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
                <script src="js/lightbox.js"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <script>

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

        </script>

    </head>
    <body>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{route('pagina.inicial')}}"> Página Inicial </a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Ligas <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('lista.ligas')}}">Lista de Ligas</a></li>
                            <li><a href="{{route('lista.times')}}">Lista de Times</a></li>
                            <li><a href="{{route('lista.jogos')}}">Lista de Jogos</a></li>
                            <li><a href="{{route('lista.jogadores')}}">Lista de Jogadores</a></li>



                        </ul>




                    </li>



<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Pontuação Por Ligas <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            @foreach($ligas as $liga)
                            <li><a href="{{route('lista.placares', $liga->id)}}">{{$liga->nome_liga}}</a></li>
                            @endforeach


                        </ul>

                        


                    </li>



<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Sub-Ligas<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('lista.sub.ligas')}}">Lista de Sub-Ligas</a></li>
                            <li><a href="{{route('lista.sub.times')}}">Lista de Sub-Times</a></li>
                            <li><a href="#">Lista de Sub-Jogos</a></li>


                        </ul>




                    </li>






<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Pontuação Por Sub-Ligas <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            
                            @foreach($subligas as $subliga)
                            <li><a href="{{route('lista.sub.placares', $subliga->id)}}">{{$subliga->nome_sub_liga}}</a></li>
                            @endforeach
                            

                        </ul>

                        


                    </li>






                </ul>

            </div>
        </nav>
        <div class="container">
            @yield('conteudo')
        </div>
    </body>
</html>