@extends('principal')

@section('conteudo')


<div class='col-sm-11'>
@if ($acao == 1)
    <h2> Inclusão de Ligas </h2>
@else
    <h2> Alteração de Ligas </h2>
@endif
</div>
<div class='col-sm-1'>
    <a href="#" class="btn btn-primary" 
       role="button">Voltar</a>
</div>

<div class='col-sm-12'>
    
@if ($acao == 1)
    <form method="post" action="{{route('salvar.liga')}}" enctype="multipart/form-data">
@else
    <form method="post" action="#">
@endif
        {{ csrf_field() }}

<div class="row">

<div class="col-sm-6">

        <div class="form-group">
            <label for="nome_liga">Nome da Liga:</label>
            <input type="text" class="form-control" id="nome_liga"
                   name="nome_liga" 
                   value="{{$liga->nome_liga or old('nome_liga')}}"
                   required>
        </div>





        <div class="form-group">
            <label for="imagem_liga"> Imagem da Liga: </label>
            <input type="file" id="imagem_liga" name="imagem_liga" 
                   onchange="previewFile()"
                   class="form-control">
        </div>

</div>

<div class="col-sm-6">
  
  {!!"<img src='imagens_ligas/sem_foto.png' id='imagem' width='200' height='175' alt='Foto da Liga'>"!!}


</div>


</div>


<script>
function previewFile() {
    var preview = document.getElementById('imagem');
    var file    = document.getElementById('imagem_liga').files[0];
    var reader  = new FileReader();
    
    reader.onloadend = function() {
        preview.src = reader.result;
    };
    
    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }    
}

</script>  




      
        <button type="submit" class="btn btn-primary">Salvar</button>        
        <button type="reset" class="btn btn-warning">Limpar</button>        
    </form>    
</div>    

@endsection