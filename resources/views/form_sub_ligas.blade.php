@extends('principal')

@section('conteudo')


<div class='col-sm-11'>
@if ($acao == 1)
    <h2> Inclusão de Sub-Ligas </h2>
@else
    <h2> Alteração de Sub-Ligas </h2>
@endif
</div>
<div class='col-sm-1'>
    <a href="#" class="btn btn-primary" 
       role="button">Voltar</a>
</div>

<div class='col-sm-12'>
    
@if ($acao == 1)
    <form method="post" action="{{route('salvar.sub.liga')}}" enctype="multipart/form-data">
@else
    <form method="post" action="#">
@endif
        {{ csrf_field() }}

<div class="row">

<div class="col-sm-6">

        <div class="form-group">
            <label for="nome_sub_liga">Nome da Sub-Liga:</label>
            <input type="text" class="form-control" id="nome_sub_liga"
                   name="nome_sub_liga" 
                   value="{{$liga->nome_sub_liga or old('nome_sub_liga')}}"
                   required>
        </div>

 <div class="form-group">
            <label for="cidade_id">Cidade da Liga:</label>
            <select class="form-control" id="cidade_id" name="cidade_id">
            <option></option>
            @foreach($cidades as $cidade)
            <option value="{{$cidade->id}}" name="nome_cidade">{{$cidade->nome_cidade}}</option>
            @endforeach
            </select>
        </div>



        <div class="form-group">
            <label for="imagem_sub_liga"> Imagem da Liga: </label>
            <input type="file" id="imagem_sub_liga" name="imagem_sub_liga" 
                   onchange="previewFile()"
                   class="form-control">
        </div>




</div>

<div class="col-sm-6">
  
  {!!"<img src='imagens_sub_ligas/sem_foto.png' id='imagem' width='200' height='175' alt='Foto da SubLiga'>"!!}


</div>


</div>


<script>
function previewFile() {
    var preview = document.getElementById('imagem');
    var file    = document.getElementById('imagem_sub_liga').files[0];
    var reader  = new FileReader();
    
    reader.onloadend = function() {
        preview.src = reader.result;
    };
    
    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }    
}

</script>  




      
        <button type="submit" class="btn btn-primary">Salvar</button>        
        <button type="reset" class="btn btn-warning">Limpar</button>        
    </form>    
</div>    

@endsection