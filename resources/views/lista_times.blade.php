@extends('principal')

@section('conteudo')

<div class='col-sm-11'>
    <h2> Times </h2>
</div>
<div class='col-sm-1'>
    <br>
    <a href="{{route('lista.times')}}" class="btn btn-primary" 
       role="button">Ver Todos</a>
</div>

<form method="post" action="{{route('pesquisar.times')}}">
    {{ csrf_field() }}

    <div class='col-sm-4'>
        <div class="form-group">
            <label for="nome_time">Pesquisa por Nome da Liga:</label>
            <select class="form-control" id="liga_id" name="liga_id">
            <option></option>
            @foreach($ligas as $liga)
            <option value="{{$liga->id}}" name="liga_id">{{$liga->nome_liga}}</option>
            @endforeach
            </select>
        </div>
    </div>


    <div class='col-sm-4'>
        <label> &nbsp; </label>
        <button type="submit" class="btn btn-warning">Pesquisar</button>
        <a href="{{route('cadastra.times')}}" class="btn btn-success">Novo Time</a>
                <a href="#" class="btn btn-info">Gerar Pdf</a>
        
    </div>

      


</form>



<div class='col-sm-12'>

    @if (count($times)==0)
    <div class="alert alert-danger">
        Não há times com os filtros informados...
    </div>
    @endif

    <table class="table table-hover">
        <thead>
            <tr>
                <th>Código</th>
                <th>Nome do Time</th>
                <th>Liga Vinculada</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($times as $time)
            <tr>
                <td style="text-align: center">{{$time->id}}</td>
                <td>{{$time->nome_time}}</td>
<td>{{$time->ligas->nome_liga}}</td>


                <td>

                    <a href="#" 
                       class="btn btn-warning" 
                       role="button">Editar</a> &nbsp;&nbsp;
                    <form style="display: inline-block"
                          method="post"
                          action="{{route('deletar.time', $time->id)}}"
                          onsubmit="return confirm('Confirma Exclusão do Time?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger"> Excluir </button>
                    </form> &nbsp;&nbsp;

                </td>
                @endforeach
            </tr>


        </tbody>
    </table>    

                    {{ $times->links() }}



</div>

@endsection