@extends('principal')

@section('conteudo')


<div class='col-sm-11'>
@if ($acao == 1)
    <h2> Inclusão de Jogadores </h2>
@else
    <h2> Alteração de Jogadores </h2>
@endif
</div>
<div class='col-sm-1'>
    <a href="#" class="btn btn-primary" 
       role="button">Voltar</a>
</div>

<div class='col-sm-12'>
    
@if ($acao == 1)
    <form method="post" action="{{route('salvar.jogador')}}">
@else
    <form method="post" action="#">
@endif
        {{ csrf_field() }}


        <div class="form-group">
            <label for="nome_jogador">Nome do Jogador:</label>
            <input type="text" class="form-control" id="nome_jogador"
                   name="nome_jogador" 
                   value="{{$jogador->nome_jogador or old('nome_jogador')}}"
                   required>
        </div>


<div class="form-group">
            <label for="time_id">Nome do Time e Liga :</label>
            <select class="form-control" id="time_id" name="time_id">
            <option></option>
            @foreach($times as $time)
            <option value="{{$time->id}}" name="nome_time"> {{$time->nome_time}} - {{$time->ligas->nome_liga}}</option>
            @endforeach
            </select>
        </div>



      

   

        

              
<script>

/*
$(function() {
  $('#time_id').change(function() {
    dropdownval = $(this).val();
    $('#time_id_2').not(this).find('option[value="' + dropdownval + '"]').hide();
  });
});
*/






                                $('#liga_id').on('change', function () {
                                    var ligaID = $(this).val();
                                    if (ligaID) {
                                        $.ajax({
                                            type: "GET",
                                            url: "{{url('ajax/pegar-times-liga')}}?liga_id=" + ligaID,
                                            success: function (res) {
                                                if (res) {
                                                    $("#time_id").empty();
                                                    $.each(res, function (key, value) {
                                                        $("#time_id").append('<option value="' + key + '">' + value + '</option>');

                                                                $("#time_id").removeAttr("disabled");



                                                               






                                                    });
                                                    
                                                } else {
                                                    $("#time_id").empty();
                                                }
                                            }
                                        });
                                    } else {
                                        $("#time_id").empty();
                                    }

                                });




$('#liga_id').on('change', function () {
                                    var ligaID = $(this).val();
                                    if (ligaID) {
                                        $.ajax({
                                            type: "GET",
                                            url: "{{url('ajax/pegar-times-liga')}}?liga_id=" + ligaID,
                                            success: function (res) {
                                                if (res) {
                                                    $("#time_id_2").empty();
                                                    $.each(res, function (key, value) {
                                                        $("#time_id_2").append('<option value="' + key + '">' + value + '</option>');

                                                                $("#time_id_2").removeAttr("disabled");


                                                    });
                                                    
                                                } else {
                                                    $("#time_id_2").empty();
                                                }
                                            }
                                        });
                                    } else {
                                        $("#time_id_2").empty();
                                    }

                                });








</script>




      
        <button type="submit" class="btn btn-primary">Salvar</button>        
        <button type="reset" class="btn btn-warning">Limpar</button>        
    </form>    
</div>    

@endsection