@extends('principal')

@section('conteudo')


<div class='col-sm-12'>
    <h1> Bem-Vindo ao Sistema de Controle de Jogos </h1>
</div>

<div class='col-sm-12'>
    <h3> <b>Desenvolvido por :</b> Renato Silva </h3>
    <h3> <b>Projetos :</b> <a href="https://www.facebook.com/Code-Hardware-Solutions-1030962670386287/?ref=page_internal"> Code & Hardware Solutions </a></h3>
</div>

<div class='col-sm-3'>
    <h5> <a href="https://www.facebook.com/RenaatoSiilva"><img src='imagens/Facebook.png' alt='Facebook'></a> </h5>
</div>


<div class='col-sm-3'>
    <h5> <a href="mailto:renato_silva94@live.com"><img src='imagens/Email.png' alt='Email'></a> </h5>
</div>

<div class='col-sm-3'>
    <h5> <a href="https://twitter.com/RenaatoSiilva"><img src='imagens/Twitter.png' alt='Twitter'></a> </h5>
</div>

<div class='col-sm-3'>
    <h5> <a href="https://www.instagram.com/renatosilva94/"><img src='imagens/Instagram.png' alt='Instagram'></a> </h5>
</div>

<div class='col-sm-12'>


</div>

@endsection
