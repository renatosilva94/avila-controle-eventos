@extends('principal')

@section('conteudo')


<div class='col-sm-11'>
@if ($acao == 1)
    <h2> Inclusão de Times </h2>
@else
    <h2> Alteração de Times </h2>
@endif
</div>
<div class='col-sm-1'>
    <a href="#" class="btn btn-primary" 
       role="button">Voltar</a>
</div>

<div class='col-sm-12'>
    
@if ($acao == 1)
    <form method="post" action="{{route('salvar.time')}}">
@else
    <form method="post" action="#">
@endif
        {{ csrf_field() }}

        <div class="form-group">
            <label for="nome_time">Nome do Time:</label>
            <input type="text" class="form-control" id="nome_time"
                   name="nome_time" 
                   value="{{$time->nome_time or old('nome_time')}}"
                   required>
        </div>

                <div class="form-group">
            <label for="liga_id">Nome da Liga Que o Time Está:</label>
            <select class="form-control" id="liga_id" name="liga_id">
            <option></option>
            @foreach($ligas as $liga)
            <option value="{{$liga->id}}" name="nome_liga">{{$liga->nome_liga}}</option>
            @endforeach
            </select>
        </div>


      
        <button type="submit" class="btn btn-primary">Salvar</button>        
        <button type="reset" class="btn btn-warning">Limpar</button>        
    </form>    
</div>    

@endsection