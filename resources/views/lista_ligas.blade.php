@extends('principal')

@section('conteudo')

<div class='col-sm-11'>
    <h2> Ligas </h2>
</div>
<div class='col-sm-1'>
    <br>
    <a href="#" class="btn btn-primary" 
       role="button">Ver Todos</a>
</div>

<form method="post" action="{{route('pesquisar.ligas')}}">
    {{ csrf_field() }}

    <div class='col-sm-4'>
        <div class="form-group">
            <label for="nome_liga">Pesquisa por Nome da Liga:</label>
            <input type="text" class="form-control" id="nome_time"
                   name="nome_liga">
        </div>
    </div>


    <div class='col-sm-4'>
        <label> &nbsp; </label>
        <button type="submit" class="btn btn-warning">Pesquisar</button>
        <a href="{{route('cadastra.ligas')}}" class="btn btn-success">Nova Liga</a>
                <a href="#" class="btn btn-info">Gerar Pdf</a>

    </div>    

</form>



<div class='col-sm-12'>

    @if (count($ligas)==0)
    <div class="alert alert-danger">
        Não há ligas com os filtros informados...
    </div>
    @endif

    <table class="table table-hover">
        <thead>
            <tr>
                <th>Código</th>
                <th>Nome da Liga</th>
                <th>Logotipo</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($ligas as $liga)
            <tr>
                <td style="text-align: center">{{$liga->id}}</td>
                <td>{{$liga->nome_liga}}</td>
                <td>
                    

                    @php        
    if(file_exists(public_path('imagens_ligas/'.$liga->id.'.png'))){
       $imagem_liga = '../imagens_ligas/'.$liga->id.'.png';
    } else {
       $imagem_liga = '../imagens_ligas/sem_foto.png';    
    }     
@endphp 

{!!"<a href=$imagem_liga data-lightbox='roadtrip' data-lightbox-gallery='gallery1' data-lightbox-hidpi=$imagem_liga>"!!}
{!!"<img src=$imagem_liga id='imagem' width='110' height='100' alt='Imagem da Liga' data-zoom-image=$imagem_liga>"!!}
</a>

                </td>


                <td>
                    <a href="#" 
                       class="btn btn-warning" 
                       role="button">Editar</a> &nbsp;&nbsp;
                    <form style="display: inline-block"
                          method="post"
                          action="{{route('deletar.liga', $liga->id)}}"
                          onsubmit="return confirm('Confirma Exclusão da Liga?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger"> Excluir </button>
                    </form> &nbsp;&nbsp;

                </td>
                @endforeach
            </tr>


        </tbody>
    </table>    

                    {{ $ligas->links() }}



</div>

@endsection