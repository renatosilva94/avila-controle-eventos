@extends('principal')

@section('conteudo')

<div class='col-sm-11'>
    <h2> Jogos </h2>
</div>
<div class='col-sm-1'>
    <br>
    <a href="{{route('lista.jogos')}}" class="btn btn-primary" 
       role="button">Ver Todos</a>
</div>

<form method="get" action="{{route('pesquisar.jogos')}}">
    {{ csrf_field() }}



<div class='col-sm-4'>
        <div class="form-group">
            <label for="liga_id">Liga:</label>
            <select class="form-control" id="liga_id" name="liga_id">
            <option></option>
            @foreach($ligas as $liga)
            <option value="{{$liga->id}}" name="liga_id">{{$liga->nome_liga}}</option>
            @endforeach
            </select>
        </div>
    </div>


   
    <div class='col-sm-4'>
        <label> &nbsp; </label>
        <button type="submit" class="btn btn-warning">Pesquisar</button>
        <a href="{{route('cadastra.jogos')}}" class="btn btn-success">Novo Jogo</a>
                <a href="#" class="btn btn-info">Gerar Pdf</a>

    </div>    

</form>



<div class='col-sm-12'>

    @if (count($jogos)==0)
    <div class="alert alert-danger">
        Não há jogos com os filtros informados...
    </div>
    @endif

    <table class="table table-hover">
        <thead>
            <tr>
                <th style="text-align: center">Código</th>
                <th style="text-align: center">Data do Jogo</th>

                
                <th style="text-align: center">Time da Casa</th>
                <th style="text-align: center">Placar do Time da Casa</th>
                <th style="text-align: center">VS</th>
                <th style="text-align: center">Placar do Time Adversário</th>
                <th style="text-align: center">Time Adversário</th>
                
                <th style="text-align: center">Liga</th>
                
                
                

                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($jogos as $jogo)
            <tr>
                <td style="text-align: center">{{$jogo->id}}</td>
                <td style="text-align: center">{{date('d-m-Y', strtotime($jogo->data_jogo))}}</td>
                <td style="text-align: center">{{$jogo->time_casa_nome->nome_time}}</td>
                <td style="text-align: center">{{$jogo->placar_time_casa}}</td>
                <th style="text-align: center">X</th>
                <td style="text-align: center">{{$jogo->placar_time_adversario}}</td>
                <td style="text-align: center">{{$jogo->time_adversario_nome->nome_time}}</td>
                <td style="text-align: center; color: #006400"><b>{{$jogo->ligas->nome_liga}}</b></td>
                
                
                
                <td>

                     <a href="{{route('desfazer.jogo', $jogo->id)}}" 
                       class="btn btn-primary" 
                       role="button">Desfazer Jogo</a> &nbsp;&nbsp;

                </td>
            @endforeach
            </tr>
            


        </tbody>
    </table>    

                    {{ $jogos->links() }}


</div>

@endsection