@extends('principal')

@section('conteudo')

<div class='col-sm-11'>
    <h2> Sub-Ligas </h2>
</div>
<div class='col-sm-1'>
    <br>
    <a href="#" class="btn btn-primary" 
       role="button">Ver Todos</a>
</div>

<form method="post" action="#">
    {{ csrf_field() }}

    <div class='col-sm-4'>
        <div class="form-group">
            <label for="nome_sub_liga">Pesquisa por Nome da Sub-Liga:</label>
            <input type="text" class="form-control" id="nome_sub_liga"
                   name="nome_sub_liga">
        </div>
    </div>


    <div class='col-sm-4'>
        <label> &nbsp; </label>
        <button type="submit" class="btn btn-warning">Pesquisar</button>
        <a href="{{route('cadastra.sub.ligas')}}" class="btn btn-success">Nova Sub-Liga</a>
                <a href="#" class="btn btn-info">Gerar Pdf</a>

    </div>    

</form>



<div class='col-sm-12'>

    @if (count($subligas)==0)
    <div class="alert alert-danger">
        Não há subligas com os filtros informados...
    </div>
    @endif

    <table class="table table-hover">
        <thead>
            <tr>
                <th>Código</th>
                <th>Nome da Sub-Liga</th>
                <th>Cidade</th>
                <th>Foto</th>
                <th>Logotipo</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($subligas as $subliga)
            <tr>
                <td style="text-align: center">{{$subliga->id}}</td>
                <td>{{$subliga->nome_sub_liga}}</td>
                <td>{{$subliga->cidades->nome_cidade}}</td>
                <td>
                    

                    @php        
    if(file_exists(public_path('imagens_sub_ligas/'.$subliga->id.'.png'))){
       $imagem_sub_liga = '../imagens_sub_ligas/'.$subliga->id.'.png';
    } else {
       $imagem_sub_liga = '../imagens_sub_ligas/sem_foto.png';    
    }     
@endphp 

{!!"<a href=$imagem_sub_liga data-lightbox='roadtrip' data-lightbox-gallery='gallery1' data-lightbox-hidpi=$imagem_sub_liga>"!!}
{!!"<img src=$imagem_sub_liga id='imagem' width='110' height='100' alt='Imagem da Sub-Liga' data-zoom-image=$imagem_sub_liga>"!!}
</a>

                </td>


                <td>
                    <a href="#" 
                       class="btn btn-warning" 
                       role="button">Editar</a> &nbsp;&nbsp;
                    <form style="display: inline-block"
                          method="post"
                          action="#"
                          onsubmit="return confirm('Confirma Exclusão da Sub-Liga?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger"> Excluir </button>
                    </form> &nbsp;&nbsp;

                </td>
                @endforeach
            </tr>


        </tbody>
    </table>    

                    {{ $subligas->links() }}



</div>

@endsection