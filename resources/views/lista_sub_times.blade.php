@extends('principal')

@section('conteudo')

<div class='col-sm-11'>
    <h2> Sub-Times </h2>
</div>
<div class='col-sm-1'>
    <br>
    <a href="{{route('lista.sub.times')}}" class="btn btn-primary" 
       role="button">Ver Todos</a>
</div>

<form method="post" action="#">
    {{ csrf_field() }}

    <div class='col-sm-4'>
        <div class="form-group">
            <label for="nome_sub_liga">Pesquisa por Nome da SubLiga:</label>
            <select class="form-control" id="liga_id" name="subliga_id">
            <option></option>
            @foreach($subligas as $subliga)
            <option value="{{$subliga->id}}" name="subliga_id">{{$subliga->nome_sub_liga}}</option>
            @endforeach
            </select>
        </div>
    </div>


    <div class='col-sm-4'>
        <label> &nbsp; </label>
        <button type="submit" class="btn btn-warning">Pesquisar</button>
        <a href="#" class="btn btn-success">Novo SubTime</a>
                <a href="#" class="btn btn-info">Gerar Pdf</a>
        
    </div>

      


</form>



<div class='col-sm-12'>

    @if (count($subtimes)==0)
    <div class="alert alert-danger">
        Não há subtimes com os filtros informados...
    </div>
    @endif

    <table class="table table-hover">
        <thead>
            <tr>
                <th>Código</th>
                <th>Nome do SubTime</th>
                <th>SubLiga Vinculada</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($subtimes as $subtime)
            <tr>
                <td style="text-align: center">{{$subtime->id}}</td>
                <td>{{$subtime->nome_sub_time}}</td>
<td>{{$subtime->subligas->nome_sub_liga}}</td>


                <td>

                    <a href="#" 
                       class="btn btn-warning" 
                       role="button">Editar</a> &nbsp;&nbsp;
                    <form style="display: inline-block"
                          method="post"
                          action="#"
                          onsubmit="return confirm('Confirma Exclusão do SubTime?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit"
                                class="btn btn-danger"> Excluir </button>
                    </form> &nbsp;&nbsp;

                </td>
                @endforeach
            </tr>


        </tbody>
    </table>    

                    {{ $subtimes->links() }}



</div>

@endsection