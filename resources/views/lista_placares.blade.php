@extends('principal')

@section('conteudo')

<div class='col-sm-11'>
    <h2> Placares - {{$nome_liga}} </h2>
</div>



   
    <div class='col-sm-4'>
        <label> &nbsp; </label>
                <a href="{{route('pdf.lista.placares', $id)}}" class="btn btn-info">Gerar Pdf</a>

    </div>    



<div class='col-sm-12'>

    @if (count($placares)==0)
    <div class="alert alert-danger">
        Não há placares com os filtros informados...
    </div>
    @endif

    <table class="table table-hover">
        <thead>
            <tr>

                <th>Classificação</th>
                <th>Nome do Time</th>

                <th>Pontos</th>
                <th>Jogos</th>
                <th>Vitórias</th>
                <th>Empates</th>
                <th>Derrotas</th>
                <th>Gols Pró</th>
                <th>Gols Contra</th>
                <th>Saldo de Gols</th>
      
          

            </tr>
        </thead>
        <tbody>

           
    <?php $i = 0 ?>

            @foreach($placares as $placar)
                <?php $i++ ?>

            <tr>
                <td>{{$i}}</td>
                <td>{{$placar->times->nome_time}}</td>
                <td>{{$placar->pontos}}</td>
                <td>{{$placar->jogos}}</td>
                <td>{{$placar->vitorias}}</td>
                <td>{{$placar->empates}}</td>
                <td>{{$placar->derrotas}}</td>
                <td>{{$placar->gols_pro}}</td>
                <td>{{$placar->gols_contra}}</td>
                <td>{{$placar->gols_pro - $placar->gols_contra}}</td>
                 

                 
                
            @endforeach
            </tr>
            


        </tbody>
    </table>    

                        



</div>

@endsection