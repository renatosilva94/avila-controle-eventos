<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pagina_inicial');
});

Route::get('pagina.inicial', 'AvilaEventosController@PaginaInicial')->name('pagina.inicial');

Route::get('lista.times', 'AvilaEventosController@PaginaListaTimes')->name('lista.times');

Route::get('lista.jogos', 'AvilaEventosController@PaginaListaJogos')->name('lista.jogos');

Route::get('cadastra.times', 'AvilaEventosController@PaginaCadastraTimes')->name('cadastra.times');

Route::post('salvar.time', 'AvilaEventosController@SalvarTime')->name('salvar.time');

Route::get('cadastra.jogos', 'AvilaEventosController@PaginaCadastraJogos')->name('cadastra.jogos');

Route::post('salvar.jogo', 'AvilaEventosController@SalvarJogo')->name('salvar.jogo');

Route::get('lista.placares/{id}', 'AvilaEventosController@PaginaListaPlacares')->name('lista.placares');

Route::get('lista.ligas', 'AvilaEventosController@PaginaListaLigas')->name('lista.ligas');

Route::get('cadastra.ligas', 'AvilaEventosController@PaginaCadastraLigas')->name('cadastra.ligas');

Route::post('salvar.liga', 'AvilaEventosController@SalvarLiga')->name('salvar.liga');

Route::get('ajax/pegar-times-liga','AvilaEventosController@getTimesPorLigaAjax');

Route::delete('deletar.time/{id}', 'AvilaEventosController@DeletarTime')->name('deletar.time');

Route::delete('deletar.liga/{id}', 'AvilaEventosController@DeletarLiga')->name('deletar.liga');

Route::get('desfazer.jogo/{id}', 'AvilaEventosController@DesfazerJogo')->name('desfazer.jogo');

Route::post('desfazer.jogo.salvar/{id}', 'AvilaEventosController@DesfazerJogoSalvar')->name('desfazer.jogo.salvar');

Route::get('pdf.lista.placares/{id}', 'AvilaEventosController@PDFListarPlacares')->name('pdf.lista.placares');

Route::post('pesquisar.times', 'AvilaEventosController@PesquisarTimes')->name('pesquisar.times');

Route::post('pesquisar.ligas', 'AvilaEventosController@PesquisarLigas')->name('pesquisar.ligas');

Route::get('pesquisar.jogos', 'AvilaEventosController@PesquisarJogos')->name('pesquisar.jogos');

Route::get('lista.sub.ligas', 'AvilaEventosController@PaginaListaSubLigas')->name('lista.sub.ligas');

Route::get('cadastra.sub.ligas', 'AvilaEventosController@PaginaCadastraSubLigas')->name('cadastra.sub.ligas');

Route::post('salvar.sub.liga', 'AvilaEventosController@SalvarSubLiga')->name('salvar.sub.liga');

Route::get('lista.sub.placares/{id}', 'AvilaEventosController@PaginaListaSubPlacares')->name('lista.sub.placares');

Route::get('lista.sub.times', 'AvilaEventosController@PaginaListaSubTimes')->name('lista.sub.times');

Route::get('lista.jogadores', 'AvilaEventosController@PaginaListaJogadores')->name('lista.jogadores');

Route::get('cadastra.jogadores', 'AvilaEventosController@PaginaCadastraJogadores')->name('cadastra.jogadores');

Route::post('salvar.jogador', 'AvilaEventosController@SalvarJogador')->name('salvar.jogador');

Route::get('adicionar.cartao.vermelho/{id}', 'AvilaEventosController@AdicionarCartaoVermelho')->name('adicionar.cartao.vermelho');

Route::get('remover.cartao.vermelho/{id}', 'AvilaEventosController@RemoverCartaoVermelho')->name('remover.cartao.vermelho');

Route::get('adicionar.cartao.amarelo/{id}', 'AvilaEventosController@AdicionarCartaoAmarelo')->name('adicionar.cartao.amarelo');

Route::get('remover.cartao.amarelo/{id}', 'AvilaEventosController@RemoverCartaoAmarelo')->name('remover.cartao.amarelo');

Route::delete('deletar.jogador/{id}', 'AvilaEventosController@DeletarJogador')->name('deletar.jogador');