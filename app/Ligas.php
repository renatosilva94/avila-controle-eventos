<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ligas extends Model {

    public $timestamps = false;
    protected $fillable = array('nome_liga');
    

}
