<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estados extends Model {

    public $timestamps = false;
    protected $fillable = array('nome_estado', 'sigla_estado');
    

}
