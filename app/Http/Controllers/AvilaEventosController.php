<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Times;
use App\Jogos;
use App\Placares;
use App\Ligas;
use App\Subligas;
use App\Cidades;
use App\Estados;
use App\Jogadores;
use Illuminate\Support\Facades\DB;
use \PDF;
use \App;
use \View;
use Illuminate\Support\Facades\Input;


class AvilaEventosController extends Controller
{
    

public function PaginaInicial(){

    $ligas = Ligas::orderBy('nome_liga')->get();
                $subligas = Subligas::orderBy('nome_sub_liga')->get();


	return view('pagina_inicial', compact('ligas', 'subligas'));
}

public function PaginaListaTimes(){

	$times = Times::paginate(10);
        $ligas = Ligas::orderBy('nome_liga')->get();
            $subligas = Subligas::orderBy('nome_sub_liga')->get();


	return view('lista_times', compact('times', 'ligas', 'subligas'));
}


public function PaginaListaPlacares($id){


        $id = Ligas::find($id)->id;
    $nome_liga = Ligas::find($id)->nome_liga;


    $times = Times::orderBy('nome_time')->get();
        $ligas = Ligas::orderBy('nome_liga')->get();
            $subligas = Subligas::orderBy('nome_sub_liga')->get();

        //Criterios de Desempate


    $placares = Placares::with('ligas', 'times')->where('liga_id', '=', $id)->orderBy('pontos', 'desc')->orderBy('vitorias', 'desc')->orderBy('saldo_gols', 'desc')->orderBy('gols_pro', 'desc')->get();
            

    return view('lista_placares', compact('times', 'placares', 'ligas', 'id', 'nome_liga', 'subligas'));
}


public function PaginaListaJogos(){


	$jogos = Jogos::paginate(10);
    $times = Times::orderBy('nome_time')->get();
    $ligas = Ligas::orderBy('nome_liga')->get();
            $subligas = Subligas::orderBy('nome_sub_liga')->get();

	return view('lista_jogos', compact('jogos', 'times', 'ligas', 'subligas'));

}

public function PaginaListaLigas(){


    $ligas = Ligas::paginate(10);
                $subligas = Subligas::orderBy('nome_sub_liga')->get();


    return view('lista_ligas', compact('ligas', 'subligas'));

}

public function PaginaCadastraTimes(){

	$acao = 1;

    $ligas = Ligas::orderBy('nome_liga')->get();
            $subligas = Subligas::orderBy('nome_sub_liga')->get();

	return view('form_times', compact('acao', 'ligas', 'subligas'));

}

public function PaginaCadastraLigas(){

    $acao = 1;

        $ligas = Ligas::orderBy('nome_liga')->get();
            $subligas = Subligas::orderBy('nome_sub_liga')->get();


    return view('form_ligas', compact('acao', 'ligas', 'subligas'));

}


public function PaginaCadastraJogos(){

	$acao = 1;
	$times = Times::orderBy('nome_time')->get();
    $ligas = Ligas::orderBy('nome_liga')->get();
            $subligas = Subligas::orderBy('nome_sub_liga')->get();

	return view('form_jogos', compact('acao', 'times', 'ligas', 'subligas'));

}


public function SalvarTime(Request $request){

	    $times = Times::create([
                    'nome_time' => $request['nome_time'],
                    'liga_id' => $request['liga_id']
        ]);

        if ($times) {

            DB::table('placares')->insert(
            ['time_id' => $times->id, 'pontos' => 0, 'jogos' => 0, 'vitorias' => 0, 'empates' => 0, 'derrotas' => 0, 'gols_pro' => 0, 'gols_contra' => 0, 'saldo_gols' => 0, 'liga_id' => $request['liga_id']]
            );

            return redirect()->route('lista.times');
        }
    

}




public function SalvarLiga(Request $request){

        $ligas = Ligas::create([
                    'nome_liga' => $request['nome_liga']
        ]);

        if ($ligas) {


            $ultimoId = $ligas->id;
            $imagemLigaId = $ultimoId . '.png';
            $request->imagem_liga->move(public_path('imagens_ligas/'), $imagemLigaId);


            return redirect()->route('lista.ligas');
        }
    

}


public function SalvarJogo(Request $request){

	    $jogos = Jogos::create([
                    'data_jogo' => $request['data_jogo'],
            'time_casa_id' => $request['time_casa_id'],
                        'placar_time_casa' => $request['placar_time_casa'],
                                    'time_adversario_id' => $request['time_adversario_id'],
                                                'placar_time_adversario' => $request['placar_time_adversario'],
                                                'liga_id' => $request['liga_id']
        
        
        
                


        ]);

        if ($jogos) {

/*Gols PRO - TIME DA CASA*/

            $gols_pro_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->increment('gols_pro', $request['placar_time_casa']);

            $gols_pro_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->increment('gols_pro', $request['placar_time_adversario']);



/*Gols CONTRA - TIME ADVERSARIO*/

            $gols_contra_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->increment('gols_contra', $request['placar_time_adversario']);

            $gols_contra_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->increment('gols_contra', $request['placar_time_casa']);
            



                        /*Contador do Total de Jogos do time da casa*/

                        $jogos_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->increment('jogos', 1);

            /*Contador do Total de Jogos do time adversário*/

                        $jogos_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->increment('jogos', 1);




            if($request['placar_time_casa'] == $request['placar_time_adversario']){

/*Pontos em caso de empate = 1 para cada time*/                

            $pontos_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->increment('pontos', 1);


            $pontos_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->increment('pontos', 1);


            /*Contador de empate de cada time*/

            $empates_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->increment('empates', 1);


            $empates_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->increment('empates', 1);


            }else if($request['placar_time_casa'] > $request['placar_time_adversario']){

/*Pontos para o time da casa caso o seu placar for maior que o seu adversário = 3*/

            $pontos_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->increment('pontos', 3);


            /*Contador de vitórias do time da casa*/

                                    $vitorias_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->increment('vitorias', 1);

            /*Contador de derrotas do time adversário*/


            $derrotas_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->increment('derrotas', 1);


            }else{

                /*Pontos para o time adversario caso o seu placar for maior que o da casa = 3*/

                        $pontos_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->increment('pontos', 3);

                        /*Contador de vitórias do time da adversario*/

                                    $vitorias_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->increment('vitorias', 1);

            /*Contador de derrotas do time da casa*/

            $derrotas_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->increment('derrotas', 1);

            }









//250







            return redirect()->route('lista.jogos');
        }
    

}



    public function getTimesPorLigaAjax(Request $request)
    {
        $timesPorLigaAjax = DB::table("times")
                    ->where("liga_id",$request->liga_id)
                    ->pluck("nome_time","id");
        return response()->json($timesPorLigaAjax);

    }


public function DeletarTime($id) {
        $time = Times::find($id);
        if ($time->delete()) {




            return redirect()->route('lista.times');
        }
    }


    public function DeletarLiga($id) {
        $liga = Ligas::find($id);
        if ($liga->delete()) {




            return redirect()->route('lista.ligas');
        }
    }



    public function DesfazerJogo($id) {
        $jogo = Jogos::find($id);

    $ligas = Ligas::orderBy('nome_liga')->get();
    $times = Times::orderBy('nome_time')->get();
            $subligas = Subligas::orderBy('nome_sub_liga')->get();


        $acao = 1;

        return view('desfazer_jogo', compact('acao', 'jogo', 'ligas', 'times', 'subligas'));

        }


            public function DesfazerJogoSalvar(Request $request, $id) {

        $this->validate($request, [
            'data_jogo' => 'required',
            'liga_id' => 'required',
            'time_casa_id' => 'required',
                        'placar_time_casa' => 'required',
                                    'time_adversario_id' => 'required',
                                                'placar_time_adversario' => 'required'
        ]);

        $dados = $request->all();

        $jogoId = Jogos::find($id);

        $alt = $jogoId->update($dados);

        if ($alt) {


/*Gols PRO - TIME DA CASA*/

            $gols_pro_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->decrement('gols_pro', $request['placar_time_casa']);

            $gols_pro_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->decrement('gols_pro', $request['placar_time_adversario']);



/*Gols CONTRA - TIME ADVERSARIO*/

            $gols_contra_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->decrement('gols_contra', $request['placar_time_adversario']);

            $gols_contra_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->decrement('gols_contra', $request['placar_time_casa']);
            


            if($request['placar_time_casa'] == $request['placar_time_adversario']){

/*Pontos em caso de empate = 1 para cada time*/                

            $pontos_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->decrement('pontos', 1);


            $pontos_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->decrement('pontos', 1);


            /*Contador de empate de cada time*/

            $empates_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->decrement('empates', 1);


            $empates_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->decrement('empates', 1);


            }else if($request['placar_time_casa'] > $request['placar_time_adversario']){

/*Pontos para o time da casa caso o seu placar for maior que o seu adversário = 3*/

            $pontos_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->decrement('pontos', 3);


            /*Contador de vitórias do time da casa*/

                                    $vitorias_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->decrement('vitorias', 1);

            /*Contador de derrotas do time adversário*/


            $derrotas_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->decrement('derrotas', 1);


            }else{

                /*Pontos para o time adversario caso o seu placar for maior que o da casa = 3*/

                        $pontos_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->decrement('pontos', 3);

                        /*Contador de vitórias do time da adversario*/

                                    $vitorias_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->decrement('vitorias', 1);

            /*Contador de derrotas do time da casa*/

            $derrotas_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->decrement('derrotas', 1);

            }

            /*Contador do Total de Jogos do time da casa*/

                        $jogos_time_casa = DB::table('placares')
            ->where('id', $request['time_casa_id'])->where('liga_id', $request['liga_id'])
            ->decrement('jogos', 1);

            /*Contador do Total de Jogos do time adversário*/

                        $jogos_time_adversario = DB::table('placares')
            ->where('id', $request['time_adversario_id'])->where('liga_id', $request['liga_id'])
            ->decrement('jogos', 1);


            DB::table('jogos')->where('id', $request['id'])->delete();


            return redirect()->route('lista.jogos');
        }
    }

    
    public function PDFListarPlacares($id){


    $id = Ligas::find($id)->id;
    $nome_liga = Ligas::find($id)->nome_liga;


    $times = Times::orderBy('nome_time')->get();
        $ligas = Ligas::orderBy('nome_liga')->get();

        //Criterios de Desempate


    $placares = Placares::with('ligas', 'times')->where('liga_id', '=', $id)->orderBy('pontos', 'desc')->orderBy('vitorias', 'desc')->orderBy('saldo_gols', 'desc')->orderBy('gols_pro', 'desc')->get();

    $pdf  =  \App::make('dompdf.wrapper');
    $view =  View::make('pdf_lista_placares', compact('placares', 'ligas', 'times', 'id', 'nome_liga'))->render();
    $pdf->loadHTML($view);
    return $pdf->stream();
}






public function PesquisarTimes(Request $request){

        $liga_id = $request->liga_id;

        $ligas = Ligas::orderBy('id')->get();
            $subligas = Subligas::orderBy('nome_sub_liga')->get();



        $filtro = array();

         if (!empty($liga_id)) {
            array_push($filtro, array('liga_id', 'like', '%' . $liga_id . '%'));
        }


        $times = Times::where($filtro)->orderBy('id')->paginate(10);

        return view('lista_times', compact('ligas', 'times', 'subligas'));



    }




public function PesquisarLigas(Request $request){

        $nome_liga = $request->nome_liga;
            $subligas = Subligas::orderBy('nome_sub_liga')->get();


        $filtro = array();

         if (!empty($nome_liga)) {
            array_push($filtro, array('nome_liga', 'like', '%' . $nome_liga . '%'));
        }


        $ligas = Ligas::where($filtro)->orderBy('id')->paginate(10);

        return view('lista_ligas', compact('ligas', 'subligas'));



    }



public function PesquisarJogos(Request $request){
            $subligas = Subligas::orderBy('nome_sub_liga')->get();

        $time_adversario_id = $request->time_adversario_id;
    $time_casa_id = $request->time_casa_id;
    $liga_id = $request->liga_id;

        $times = Times::orderBy('id')->get();
            $ligas = Ligas::orderBy('nome_liga')->get();
            $subligas = Subligas::orderBy('nome_sub_liga')->get();


        $filtro = array();

if (!empty($liga_id)) {
            array_push($filtro, array('liga_id', '=',  $liga_id));
        }



        $jogos = Jogos::where($filtro)->orderBy('id')->paginate(100);

        return view('lista_jogos', compact('jogos', 'times', 'ligas', 'subligas'));



    }



public function PaginaListaSubLigas(){

                $ligas = Ligas::orderBy('nome_liga')->get();



    $subligas = Subligas::paginate(10);
    

    return view('lista_sub_ligas', compact('subligas', 'ligas'));

}



public function PaginaCadastraSubLigas(){

    $acao = 1;
            $subligas = Subligas::orderBy('nome_sub_liga')->get();

        $ligas = Ligas::orderBy('nome_liga')->get();
        $cidades = Cidades::orderBy('nome_cidade')->get();

    return view('form_sub_ligas', compact('acao', 'ligas', 'cidades', 'subligas'));

}



public function SalvarSubLiga(Request $request){

        $subligas = Subligas::create([
                    'nome_sub_liga' => $request['nome_sub_liga'],
                    'cidade_id' => $request['cidade_id']
        ]);

        if ($subligas) {


            $ultimoId = $subligas->id;
            $imagemSubLigaId = $ultimoId . '.png';
            $request->imagem_sub_liga->move(public_path('imagens_sub_ligas/'), $imagemSubLigaId);


            return redirect()->route('lista.sub.ligas');
        }
    

}




public function PaginaListaSubTimes(){

    $subtimes = Times::paginate(10);
        $ligas = Ligas::orderBy('nome_liga')->get();
            $subligas = Subligas::orderBy('nome_sub_liga')->get();


    return view('lista_sub_times', compact('subtimes', 'ligas', 'subligas'));
}




public function PaginaListaJogadores(){


    $jogadores = Jogadores::paginate(10);

    $ligas = Ligas::orderBy('nome_liga')->get();
            $subligas = Subligas::orderBy('nome_sub_liga')->get();

    return view('lista_jogadores', compact('jogadores', 'ligas', 'subligas'));

}

public function PaginaCadastraJogadores(){

    $acao = 1;

    $ligas = Ligas::orderBy('nome_liga')->get();
            $subligas = Subligas::orderBy('nome_sub_liga')->get();
            $times = Times::orderBy('nome_time')->get();

    return view('form_jogadores', compact('acao', 'ligas', 'subligas', 'times'));

}




public function SalvarJogador(Request $request){

        $jogadores = Jogadores::create([
                    'nome_jogador' => $request['nome_jogador'],
                    'time_id' => $request['time_id'],
            'cartao_amarelo' => 0,
                        'cartao_vermelho' => 0

                             
        ]);

        if ($jogadores) {


           


            return redirect()->route('lista.jogadores');
        }
    

}






public function AdicionarCartaoVermelho(Request $request, $id) {

       

        $dados = $request->all();

        $jogadorId = Jogadores::find($id);

        $alt = $jogadorId->update($dados);

        if ($alt) {



            $adicionar_cartao_vermelho = DB::table('jogadores')
            ->where('id', $request['id'])->increment('cartao_vermelho', 1);


return redirect()->route('lista.jogadores');

        }

    }




public function RemoverCartaoVermelho(Request $request, $id) {

       

        $dados = $request->all();

        $jogadorId = Jogadores::find($id);

        $alt = $jogadorId->update($dados);

        if ($alt) {



            $remover_cartao_vermelho = DB::table('jogadores')
            ->where('id', $request['id'])->decrement('cartao_vermelho', 1);


return redirect()->route('lista.jogadores');

        }

    }









public function AdicionarCartaoAmarelo(Request $request, $id) {

       

        $dados = $request->all();

        $jogadorId = Jogadores::find($id);

        $alt = $jogadorId->update($dados);

        if ($alt) {



            $adicionar_cartao_amarelo = DB::table('jogadores')
            ->where('id', $request['id'])->increment('cartao_amarelo', 1);


return redirect()->route('lista.jogadores');

        }

    }




public function RemoverCartaoAmarelo(Request $request, $id) {

       

        $dados = $request->all();

        $jogadorId = Jogadores::find($id);

        $alt = $jogadorId->update($dados);

        if ($alt) {



            $remover_cartao_amarelo = DB::table('jogadores')
            ->where('id', $request['id'])->decrement('cartao_amarelo', 1);


return redirect()->route('lista.jogadores');

        }

    }




    public function DeletarJogador($id) {
        $jogador = Jogadores::find($id);
        if ($jogador->delete()) {




            return redirect()->route('lista.jogadores');
        }
    }


 


}
