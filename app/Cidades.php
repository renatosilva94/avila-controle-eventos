<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidades extends Model {

    public $timestamps = false;
    protected $fillable = array('nome_cidade', 'estado_id');

    

}
