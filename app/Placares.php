<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Placares extends Model {

    public $timestamps = false;
    protected $fillable = array('time_id', 'pontos', 'jogos', 'vitorias', 'empates', 'derrotas', 'gols_pro', 'gols_contra', 'saldo_gols', 'liga_id');



public function times(){
        return $this->hasOne('App\Times', 'id', 'time_id');
    }

public function ligas(){
        return $this->hasOne('App\Ligas', 'id', 'liga_id');
    }



}
