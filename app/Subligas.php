<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subligas extends Model {

    public $timestamps = false;
    protected $fillable = array('nome_sub_liga', 'cidade_id');
    

    public function cidades(){
        return $this->hasOne('App\Cidades', 'id', 'cidade_id');
    }  

}
