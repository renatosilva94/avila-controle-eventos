<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Times extends Model {

    public $timestamps = false;
    protected $fillable = array('nome_time', 'liga_id');

public function ligas(){
        return $this->hasOne('App\Ligas', 'id', 'liga_id');
    }    

}
