<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jogadores extends Model {

    public $timestamps = false;
    protected $fillable = array('nome_jogador', 'cartao_vermelho', 'cartao_amarelo' ,'time_id');


public function times(){
        return $this->hasOne('App\Times','id', 'time_id');
    }

    public function ligas(){
        return $this->hasOne('App\Ligas','id', 'liga_id');
    }



}
